<?php $page = "Gallery"; ?>
<?php include('inc_header.php');?>
    <!-- middle -->
    <!--<div class="slider-wrapper photo-detail">
      <div id="slider" class="nivoSlider">
        <img src="images/slider/banner-photography.jpg" alt="" title="#htmlcaption" />
        <img src="images/slider/banner-photography2.jpg" alt="" title="#htmlcaption2" />
        <img src="images/slider/banner-photography.jpg" alt="" title="#htmlcaption" />
        <img src="images/slider/banner-photography2.jpg" alt="" title="#htmlcaption2" />
      </div>
      <div id="htmlcaption" class="nivo-html-caption">
        <div class="text"><span>Sensl</span>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel
          <div class="date">January 2014</div>
        </div>
      </div>
      <div id="htmlcaption2" class="nivo-html-caption">
        <div class="text"><span>Sensl</span>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel
          <div class="date">January 2014</div>
        </div>
      </div>
      <nav>
        <a href="#" class="more">more</a>
        <a href="#" class="share">share</a>
      </nav>
    </div>-->
    <section id="banner-content"><img src="images/slider/banner-kawasaki.jpg" alt="">
      <div class="captions">
        <div class="brand">Kawasaki</div>
        <h2>KAWASAKI G23</h2>
        <p>COMMERCIAL  //  Photo Shoot, Video Shoot, TV Commercial, Brand Activation</p>
      </div>
    </section>
    <nav id="banner">
      <div class="left">
        <a href="#" class="more">more</a>
        <a href="#" class="share">share</a>
      </div>
      <span>KAWASAKI</span>
      <div class="right">
        <a href="#" class="prev">prev</a>
        <a href="#" class="next">next</a>
      </div>
    </nav>
    <div id="infoClient" class="afterclear">
      <div class="wrap-wide">
        <div><img src="images/material/logo-kawasaki.jpg" alt=""></div>
        <div><span>Company</span> PT. kawasaki motors</div>
        <div><span>Date Completed</span> 22 June 2010</div>
        <div><span>Location</span> Yokohama, Japan</div>
      </div>
    </div>
    <div class="wrap-wide">
      <section class="view-column two content-wording">
        <h5>THE TASK</h5>
        <div><span class="tagline">This is a sample brief description for video portfolio gallery detail.</span>
        </div>
        <div>
          <p>Information will be adjusted later according to the project. Each description will be about three to four sentences long. </p>
          <p>Kawasaki Aircraft initially manufactured motorcycles under the Meguro name, having bought out an ailing motorcycle manufacturer called Meguro Manufacturing Co. Ltd with whom they had been in partnership earlier, but later formed Kawasaki Motor Sales. Early motorcycles display an emblem with &quot;Kawasaki Aircraft&quot; on the fuel tank.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis lectus metus, at posuere neque. Sed pharetra nibh eget orci convallis at posuere leo convallis. Sed blandit augue vitae augue scelerisque bibendum. </p>
        </div>
      </section>
      <div><img src="images/content/img-kawasaki.jpg" alt="">
        <div class="footnote">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
      </div>
      <section class="view-column two content-wording">
        <h5>the result</h5>
        <div><span class="tagline">sollicitudin, lorem<br>
          quis bibendum<br>
          auctor.</span>
        </div>
        <div>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. </p>
          <p>Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. <br />
            Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa </p>
        </div>
      </section>
      <div id="video"><img src="images/content/img-video.jpg" alt="">
        <div class="footnote">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
      </div>
      <div class="display-image">
        <div><img src="images/content/img-part-kawasaki.jpg" alt=""></div>
        <div><img src="images/content/img-part-kawasaki-10.jpg" alt=""></div>
        <div><img src="images/content/img-part-kawasaki-12.jpg" alt=""></div>        
      </div>
      <div class="footnote">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
      <!--<div class="slider-wrapper img-slider">
        <div id="slider-image" class="nivoSlider">
          <img src="images/content/image-slider.jpg" alt="" />
          <img src="images/content/image-slider2.jpg" alt="" />
          <img src="images/content/image-slider.jpg" alt="" />
          <img src="images/content/image-slider2.jpg" alt="" />
        </div>
      </div>-->
    </div>
    <div class="box-getstart">
      <div class="wrap-wide">
        <p>Deliver your message and empower your brand through visual imaging</p>
        <a href="#letstalk" class="btn white right fancybox">get started</a>
      </div>
    </div>
    <div class="view-column two like-gallery">
      <div><img src="images/content/image-column.jpg" alt="">
        <div class="company-logo"><img src="images/content/img-logo-company-03.png" alt=""></div>
      </div>
      <div><img src="images/content/image-column-03.jpg" alt="">
        <div class="company-logo"><img src="images/content/img-logo-company.png" alt=""></div>
      </div>
    </div>
    <!-- end of middle -->
    <script type="text/javascript">
    $(window).load(function() {
        /*$('#slider').nivoSlider({
			directionNav: true,
			controlNav: false,			
		});
		$('#slider-image').nivoSlider({
			directionNav: true,
			controlNav: false,			
		});*/		
    });
    </script>
    <?php include('inc_footer.php');?>