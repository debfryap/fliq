<?php $page = ""; ?>
<?php include('inc_header.php');?>
    <!-- middle -->
    <section id="home" class="afterclear">
      <div class="items">
        <div class="f_image"><img src="images/slider/banner-01.jpg" alt=""/></div>
        <div class="captions">
          <h2>
            <a href="gallery-photo.php">PHOTO</a>
          </h2>
        </div>
      </div>
      <div class="items"><div class="f_video"><!--
        <video loop preload="auto" style="width: 100%; height: auto;" id="videoLoop">
          <source src="images/video/films.webm" type="video/webm">
          <source src="images/video/films.ogv" type="video/ogg">
          <source src="images/video/films.mp4" type="video/mp4">
        </video>-->
        <video src="images/video/films.mp4" loop preload="auto" id="videoLoop"></video></div>
        <div class="captions">
          <h2>
            <a href="gallery-video.php">Video</a>
          </h2>
        </div>
      </div>
    </section>
    <section class="home-info">
      <div class="wrap-wide">
        <div class="main-title">We blend art, technology, and strategy to create <span>extraordinary</span> visual content. </div>
        <p>Headquartered in Jakarta, Indonesia, FLIQ is a visualproduction companyestablished to create powerful still and moving images to reinforce brands or businesses. We are dedicated to craft visuals that are designed to communicate to the right people at the right time in exactly the right form.</p>
        <a href="service.php" class="btn blue anim"><span>View our services</span></a>
        <ul class="icon-list afterclear">
          <li>
            <div class="strategy">
              <a href="service.php">link</a>
            </div>
            <span>Strategy</span></li>
          <li>
            <div class="prepro"><a href="service.php">link</a></div>
            <span>pre-production</span></li>
          <li>
            <div class="pro"><a href="service.php">link</a></div>
            <span>production</span></li>
          <li>
            <div class="digital"><a href="service.php">link</a></div>
            <span>digital marketing</span></li>
          <li>
            <div class="reporting"><a href="service.php">link</a></div>
            <span>reporting</span></li>
        </ul>
      </div>
    </section>
    <div class="box-getstart">
      <div class="wrap-wide">
        <p>Deliver your message and empower your brand through photo & video production</p>
        <a href="#workwithus" class="btn white right fancybox">get started</a>
      </div>
    </div>
    <script>
    $(function(){		
		$("#home .items").hoverVideo();	
	})
    </script>
    <!-- end of middle -->
    <?php include('inc_footer.php');?>