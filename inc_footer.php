<!--Footer -->

<footer class="home">
  <p>©2014 FLIQ. Powered by WEBARQ</p>
  <div class="socmed-foot">
    <a href="#"><img src="images/material/icon-socmed-footer.png" width="20" height="20" alt=""/><span><img src="images/material/socmed-blue.png" width="20" height="20" alt="" /></span></a>
    <a href="#"><img src="images/material/icon-socmed-footer-02.png" width="20" height="20" alt=""/><span><img src="images/material/socmed-blue-02.png" width="20" height="20" alt="" /></span></a>
    <a href="#"><img src="images/material/icon-socmed-footer-03.png" width="20" height="20" alt=""/><span><img src="images/material/socmed-blue-03.png" width="20" height="20" alt="" /></span></a>
    <a href="#"><img src="images/material/icon-socmed-footer-04.png" width="20" height="20" alt=""/><span><img src="images/material/socmed-blue-04.png" width="20" height="20" alt="" /></span></a>
    <a href="#"><img src="images/material/icon-socmed-footer-05.png" width="20" height="20" alt=""/><span><img src="images/material/socmed-blue-05.png" width="20" height="20" alt="" /></span></a>
  </div>
</footer>
<!--end of Footer -->
</div>
<!--end of Container  wrapper-->
</div>
<!--end of Container -->
<div id="loading"></div>
<!--inner html -->
<div style="display:none">
  <div id="letstalk" class="popup">
    <div class="close"><img src="images/material/btn-close.png" alt=""></div>
    <div class="wrap_popup">
      <div class="title">let’s talk</div>
      <p>Say hello, why don’t you!</p>
      <form action="" method="post">
        <div class="row">
          <label>Name</label>
          <input name="" type="text" value="">
          <span>*</span>
        </div>
        <div class="row">
          <label>Brand / Company Name </label>
          <input name="" type="text" value="">
          <span>*</span>
        </div>
        <div class="row">
          <label>Email Address </label>
          <input name="" type="text" value="">
          <span>*</span>
        </div>
        <div class="row">
          <label>Website </label>
          <input name="" type="text" value="">
          <span></span>
        </div>
        <div class="row">
          <label>Phone Number </label>
          <textarea name="" style="resize:none;"></textarea>
          <span>*</span>
        </div>
        <div class="row">
          <label>Message </label>
          <textarea name="" style="resize:none;"></textarea>
          <span>*</span>
        </div>
        <input name="" type="submit" value="send message" class="btn blue">
      </form>
    </div>
  </div>
  <div id="workwithus" class="popup">
        <div class="close"><img src="images/material/btn-close.png" alt=""></div>
        <div class="wrap_popup">
          <div class="title">work with us</div>
          <p>Fill out this form and we'll contact you to schedule a time to talk!</p>
          <div class="circle-step"><span id="step-min">1</span>/<span id="step-max">2</span></div>
          <form action="" method="post">
            <div id="step1">
              <div class="row">
                <label>Name?</label>
                <input name="" type="text" value="">
                <span>*</span>
              </div>
              <div class="row">
                <label>Brand / Company Name </label>
                <input name="" type="text" value="">
                <span>*</span>
              </div>
              <div class="row">
                <label>Location</label>
                <input name="" type="text" value="">
                <span>*</span>
              </div>
              <div class="row">
                <label>Email Address </label>
                <input name="" type="text" value="">
                <span>*</span>
              </div>
              <div class="row">
                <label>Website </label>
                <input name="" type="text" value="">                
              </div>
              <div class="row">
                <label>Phone Number </label>
                <input name="" type="text" value="">
                <span>*</span>
              </div>
              <a href="#step2" class="btn blue" style="margin-top:20px;" id="continueForm">continue</a>
            </div>
            <div id="step2" style="display:none;">
              <div class="row">
                <label>Services Required </label>
                <ul class="icon-list afterclear inline-pop">
                  <li>
                    <div class="strategy"></div>
                    <span>strategy</span></li>
                  <li>
                    <div class="prepro"></div>
                    <span>pre-production</span></li>
                  <li>
                    <div class="pro"></div>
                    <span>production</span></li>
                  <li>
                    <div class="digital"></div>
                    <span>digital marketing</span></li>
                  <li>
                    <div class="reporting"></div>
                    <span>reporting</span></li>
                </ul>
                <input name="" type="hidden" value="" id="f_services">
              </div>
              <div class="row">
                <label>Describe Your Project</label>
                <textarea name=""></textarea>
              </div>
              <div class="row">
                <label>Your Objectives & Goals</label>
                <textarea name=""></textarea>
              </div>
              <div class="row">
                <label>Message</label>
                <textarea name=""></textarea>
              </div>
              <input name="" type="submit" value="SEND PROJECT PLANNER" class="btn blue">
            </div>
          </form>
        </div>
      </div>
</div>
</body></html>