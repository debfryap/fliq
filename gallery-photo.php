<?php $page = "PHOTO"; ?>
<?php include('inc_header.php');?>
    <!-- middle -->
    <div class="slider-wrapper theme-default">
            <div id="slider" class="nivoSlider">
                <img src="images/slider/banner-photography.jpg" alt="" title="#htmlcaption" />
                <img src="images/slider/banner-photography2.jpg" alt="" title="#htmlcaption2" />
                <img src="images/slider/banner-photography.jpg" alt="" title="#htmlcaption" />
                <img src="images/slider/banner-photography2.jpg" alt="" title="#htmlcaption2" />
      </div>
            <div id="htmlcaption" class="nivo-html-caption">
                <div class="text">PHOTO</div>
            </div>
            <div id="htmlcaption2" class="nivo-html-caption">
                <div class="text">PHOTO2</div>
            </div>
    </div>
    <!--<div class="wrap-wide">
      <section class="view-column two content-wording">
        <div class=""><span class="tagline">This is Photoshop's version  of Lorem<br>Ipsum Proin gravida<br>nibh vel velit auctor aliquet. Aenean </span>
        </div>
        <div>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
<p>Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
        </div>
      </section>
    </div>-->
    <section id="main-gallery">
      
      <nav><a href="#" class="btn sort active"><span>ALL</span></a>
      <a href="#" class="btn sort"><span>ADVERTISING</span></a>
      <a href="#" class="btn sort"><span>ARCHITECTURE</span></a>
      <a href="#" class="btn sort"><span>FINE ART</span></a>
      <a href="#" class="btn sort"><span>EVENTS</span></a>
      <a href="#" class="btn sort"><span>NATURE</span></a>
      <!--<a href="#" class="more">MORE</a>-->
      </nav>
      <ul class="list-gallery afterclear main-gallery">
        <li><img src="images/content/img-gallery.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company.png" alt=""></div>
          <div class="hover with-text">
          	<div class="wrap-text">
            	<span class="name">Sensl</span>
                <div class="main-cap">This is Photoshop's version <br>of Lorem Ipsum. </div>
                <span class="date">January 2014</span>
             </div>
           </div>
        </li>
        <li><img src="images/content/img-gallery-02.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-02.png" alt=""></div>
          <div class="hover with-text">
          	<div class="wrap-text">
            	<span class="name">Sensl</span>
                <div class="main-cap">This is Photoshop's version <br>of Lorem Ipsum. </div>
                <span class="date">January 2014</span>
             </div>
           </div>
        </li>
        <li><img src="images/content/img-gallery-03.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-03.png" alt=""></div>
          <div class="hover with-text">
          	<div class="wrap-text">
            	<span class="name">Sensl</span>
                <div class="main-cap">This is Photoshop's version <br>of Lorem Ipsum. </div>
                <span class="date">January 2014</span>
             </div>
           </div>
        </li>
        <li><img src="images/content/img-gallery.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company.png" alt=""></div>
          <div class="hover with-text">
          	<div class="wrap-text">
            	<span class="name">Sensl</span>
                <div class="main-cap">This is Photoshop's version <br>of Lorem Ipsum. </div>
                <span class="date">January 2014</span>
             </div>
           </div>
        </li>
        <li><img src="images/content/img-gallery-02.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-02.png" alt=""></div>
          <div class="hover with-text">
          	<div class="wrap-text">
            	<span class="name">Sensl</span>
                <div class="main-cap">This is Photoshop's version <br>of Lorem Ipsum. </div>
                <span class="date">January 2014</span>
             </div>
           </div>
        </li>
        <li><img src="images/content/img-gallery-03.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-03.png" alt=""></div>
          <div class="hover with-text">
          	<div class="wrap-text">
            	<span class="name">Sensl</span>
                <div class="main-cap">This is Photoshop's version <br>of Lorem Ipsum. </div>
                <span class="date">January 2014</span>
             </div>
           </div>
        </li>
        <li><img src="images/content/img-gallery.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company.png" alt=""></div>
          <div class="hover with-text">
          	<div class="wrap-text">
            	<span class="name">Sensl</span>
                <div class="main-cap">This is Photoshop's version <br>of Lorem Ipsum. </div>
                <span class="date">January 2014</span>
             </div>
           </div>
        </li>
        <li><img src="images/content/img-gallery-02.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-02.png" alt=""></div>
          <div class="hover with-text">
          	<div class="wrap-text">
            	<span class="name">Sensl</span>
                <div class="main-cap">This is Photoshop's version <br>of Lorem Ipsum. </div>
                <span class="date">January 2014</span>
             </div>
           </div>
        </li>
        <li><img src="images/content/img-gallery-03.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-03.png" alt=""></div>
          <div class="hover with-text">
          	<div class="wrap-text">
            	<span class="name">Sensl</span>
                <div class="main-cap">This is Photoshop's version <br>of Lorem Ipsum. </div>
                <span class="date">January 2014</span>
             </div>
           </div>
        </li>
      </ul>
    </section>
    <div id="loadmore"><span>load more</span></div>
    <div class="box-getstart">
      <div class="wrap-wide">
        <p>BRING YOUR STORIES TO LIFE WITH OUR PHOTO PRODUCTION</p>
        <a href="#workwithus" class="btn white right fancybox">LET’S WORK TOGETHER </a>
      </div>
    </div>
    <!-- end of middle -->
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
			directionNav: false,
		});
    });
    </script>
    <?php include('inc_footer.php');?>