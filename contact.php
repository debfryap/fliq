<?php $page = "contact"; ?>
<?php include('inc_header.php');?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false"></script>
    <script>
function initialize()
{
var poin = new google.maps.LatLng(-6.192542,106.797777); 
var mapStyle = [
  {
    "featureType": "road.highway.controlled_access",
    "stylers": [
      { "visibility": "on" },
      { "color": "#c2c2c2" }
    ]
  },{
    "featureType": "water",
    "stylers": [
      { "visibility": "on" },
      { "color": "#47c0ef" }
    ]
  },{
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      { "visibility": "on" },
      { "color": "#a8a8a8" }
    ]
  },{
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      { "color": "#9b9b9b" }
    ]
  },{
    "featureType": "landscape.man_made",
    "stylers": [
      { "hue": "#0091ff" },
      { "color": "#f2f2f2" }
    ]
  }
];
var styledMap = new google.maps.StyledMapType(mapStyle,
    {name: "Styled Map"});
var mapProp = {
	  center: poin,
	  zoom:16,
	  panControl:false,
	  zoomControl:false,
	  mapTypeControl:false,
	  mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    }
  };
	var map=new google.maps.Map(document.getElementById("googleMaps"),mapProp);
	var marker=new google.maps.Marker({
	  position:poin,
	  icon : "images/material/pointer.png"
  });
  marker.setMap(map);
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
    <!-- middle -->
    <section id="banner-content"><img src="images/slider/banner-contact.jpg" alt="">
      <div class="captions">
        <h2>WE’D LOVE TO HEAR FROM YOU!</h2>
      </div>
    </section>
    <div class="wrap-wide">
      <section id="contact">
        <div class="title">contact</div>
        <p>We’d love to talk with you about working together on your next project or campaign, so feel free to call us or drop a word. </p>
        <ul class="list-contact afterclear">
          <li>
            <div>
              <div class="icon">
                <a href="#workwithus" class="fancybox"><img src="images/material/icon-contact.png" alt=""><img src="images/material/icon-contact-blue.png" alt=""></a>
              </div>             
              <span>Start Your Project</span>
              <a href="#workwithus" class="fancybox">Work With Us</a>
            </div>
          </li>
          <li>
            <div>
              <div class="icon">
                <a href="#letstalk" class="fancybox"><img src="images/material/icon-contact-02.png" alt=""><img src="images/material/icon-contact-02-blue.png" alt=""></a>
              </div>              
              <span>Say Hello</span>  
              <a href="#letstalk" class="fancybox">info@fliq.id</a>            
            </div>
          </li>
          <li>
            <div>
              <div class="icon">
                <a href="career.php"><img src="images/material/icon-contact-03.png" alt=""><img src="images/material/icon-contact-03-blue.png" alt=""></a>
              </div>
              <span>Careers</span>
              <a href="career.php">Join Our Team</a>
            </div>
          </li>
          <li>
            <div>
              <div class="icon"><img src="images/material/icon-contact-04.png" alt=""><img src="images/material/icon-contact-04-blue.png" alt=""></div>
              <span>Connect With Us</span>              
              <a href="#"><img src="images/material/socmed-blue-02.png" alt=""></a>
              <a href="#"><img src="images/material/socmed-blue-03.png" alt=""></a>              
            </div>
          </li>
        </ul>
        <div class="address">
          <!--<img src="images/content/maps.jpg" alt="">-->
          <div id="googleMaps"></div>
          <div class="address-tabs">
            <div><img src="images/material/fliq.png" alt=""></div>
            <div><img src="images/material/icon-location.png" alt="">
              <span class="small">MENARA CITICON LT. 15 SUITE A-B<br>
              JL. S. PARMAN KAV 72<br>
              JAKARTA 11410</span>
            </div>
            <div><img src="images/material/icon-phone.png" alt=""><span>+62 21 29308707</span></div>
            <div><img src="images/material/icon-fax.png" alt=""><span>+62 21 29308701</span></div>
            <div>
              <a href="#" class="reset_location">location</a>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="box-getstart">
      <div class="wrap-wide">
        <p>GET IN TOUCH WITH US TO LEARN HOW YOU CAN COLLABORATE WITH FLIQ</p>
        <a href="#letstalk" class="btn white right fancybox">REQUEST CONSULTATION</a>
      </div>
    </div>    
    <!-- end of middle -->
    <?php include('inc_footer.php');?>