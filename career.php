<?php $page = "careers"?>
<?php include('inc_header.php');?>
    <!-- middle -->
    <section id="banner-content"><img src="images/slider/banner-career.jpg" alt="">
      <div class="captions">
        <h2>JOIN OUR GROWING TEAM</h2>
      </div>
    </section>
    <div class="wrap-wide">
      <section class="view-column two content-wording">
        <div class=""><span class="tagline">WE ARE ALWAYS ON<br>THE LOOKOUT FOR TALENTED INDIVIDUALS WHO SHARE OUR PASSION IN PHOTO & VIDEO PRODUCTION</span>
          <p style="margin-top:15px;">Please take the time to introduce yourself!</p>
        </div>
        <div>
          <ul class="list-careers">
            <li>
              <a href="career-detail.php">Editorial Photographer</a>
              <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. </p>
            </li>
            <li>
              <a href="career-detail.php">Communications Assistant</a>
              <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquetenean sollicitudin, lorem quis bibendum auctor</p>
            </li>
            <li>
              <a href="career-detail.php">Multimedia Producer</a>
              <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. </p>
            </li>
            <li>
              <a href="career-detail.php">Still Life Retoucher</a>
              <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquetenean sollicitudin, lorem quis bibendum auctor</p>
            </li>
            <li>
              <a href="career-detail.php">Photographer</a>
              <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. </p>
            </li>
            <li>
              <a href="career-detail.php">Digital Video Editor</a>
              <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquetenean sollicitudin, lorem quis bibendum auctor </p>
            </li>
            <li>
              <a href="career-detail.php">Motion Graphics Designer</a>
              <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. </p>
            </li>
          </ul>
          <nav class="paging">
            <a href="#">1</a>
            <a href="#">2</a>
            <a href="#" class="active">3</a>
          </nav>
        </div>
      </section>
    </div>
    <section id="job">
      <div class="wrap-wide">
        <div class="title">APPLY TODAY</div>
        <p>For job and internship opportunities; submit your CV and portfolio to 
          <a href="mailto:career@fliq.id">career@fliq.id</a>
        </p>
        <img src="images/content/img-jobapp.png" alt=""></div>
    </section>
    <div class="box-getstart">
      <div class="wrap-wide">
        <p>Deliver your message and empower your brand through photo & video production</p>
        <a href="#workwithus" class="btn white right fancybox">get started</a>
      </div>
    </div>
    
    <!-- end of middle -->
    <?php include('inc_footer.php');?>