$(document).ready(function () {
	//validation ie 8
	if ($("html").is(".lt-ie9")) {
		$("body").html("<p class='chromeframe'>You are using an <strong>outdated</strong> browser.<br>Please <a href='http://outdatedbrowser.com/en' target='_blank'>upgrade your browser</a> or <a href='https://developers.google.com/chrome/chrome-frame/' target='_blank'>activate Google Chrome Frame</a> to improve your experience.</p>")
	}
	var device = "";	
	$(window).on("load resize", function () {
		var size_mobile = 480;
		var size_tablet = 1024;
		var size_desktop = 1366;
		var size_browser = $(this).width();
		if (size_browser >= size_desktop) {
			desktop();
		} else if (size_browser > size_mobile && size_browser <= size_tablet) {
			tablet()
		} else if (size_browser <= size_mobile) {
			mobile();
		}
		return false;
	});
	$(window).load(function (e) {
		$("#loading").velocity({
			top : "100%",
		}, {
			duration : 1000,
			easing : "easeInOutQuad",
			complete : function () {
				$(this).remove();
				$("#home .items").height($("#home .items").find("img:first").height());
				$("#home").height($("#home .items").find("img:first").height());
				//set size caption home
				$("#home .items .captions").each(function (index) {
					$(this).css({
						'width' : $(this).find("h2").outerWidth() + "px",
						'height' : $(this).find("h2").outerHeight() + "px",
						'opacity' : '1',
					});
				});

				banner_content();
				//set css video
				$("#home .items video").css({
					'opacity' : 1,
					'width' : 'auto',
					'height' : '100%'
				});
			}
		});
		$("#container").show();
		//scroll paralax
		var sc = 0;
		if ($("#home").length > 0) {
			var end = $("#home .items").find("img:first").height();
		} else {
			var end = 900;
		}
		var FIREFOX = /Firefox/i.test(navigator.userAgent);
		
		if (FIREFOX) {
			var speed_sc = 50;
			
		} else {
			var speed_sc = 1;
		}

		$(window).mousewheel(function (event, delta) {
			if (delta > 0) {
				sc  =0;
				if (sc <= 70) {
					sc = 0;
				}
				console.log();
				$(".f_image,.f_video").css('top',sc+'px');
			} else {
				sc  = $(window).scrollTop();
				if (sc >= end) {
					sc = end;
				}
				console.log(sc + "down" + end);
				$(".f_image,.f_video").velocity({
				top : sc + "px",
			}, {
				duration : speed_sc,
			})
			}
			
		})

	});
	$(window).scroll(function () {
		if ($(this).scrollTop() > 71) {
			$("header").css('position', 'fixed');
			console.log
		} else {
			$("header").css('position', 'relative');
		}

		// nav on top
		if ($("nav#banner").length > 0) {
			if ($(this).scrollTop() >= $("#banner-content").height() - 100) {
				$("header").css('opacity', '0');
				$("nav#banner").css('position', 'fixed');
				console.log($(this).scrollTop() + "X" + $("#infoClient").offset().top)
			} else {
				$("header").css('opacity', '1');
				$("nav#banner").removeAttr('style');
			}
		}

	});

	function mobile() {
		device = "Mobile";
	}
	function tablet() {
		device = "Tablet";
	}
	function desktop() {
		device = "Desktop";
	}
	if (!Modernizr.cssanimations) {}
	$('.fancybox').fancybox({
		padding : '0px',
		autoSize : false,
		openEffect : 'fade',
		helpers : {
			overlay : {
				locked : false
			}
		},
		afterLoad : function () {
			$('html').css('overflow', 'hidden');
			inAnimateForm();
		},
		afterClose : function () {
			$('html').removeAttr('style');
		},
		beforeClose : function () {}
	});
	$.fn.hoverVideo = function () {

		$(this).hover(function () {
			if ($(this).children(".f_image").length == 1) {
				$(this).find("img").velocity({
					scale : "1.3,1.3"
				}, {
					duration : 15000
				});
			} else {
				document.getElementById("videoLoop").play();
			}

		}, function () {

			if ($(this).find("img").length == 1) {
				$(this).find("img").velocity({
					scale : "1,1"
					}, {
					duration : 1000
				})
			} else {
				document.getElementById("videoLoop").pause();
			}

		});

	}

	//select icon service
	$(".inline-pop li").click(function () {
		var value = $(this).find("span").text();
		$("#f_services").val(value);
		$(".inline-pop li").removeClass('active');
		$(this).addClass('active');
	});
	$(".wrap-file input").on("change", function () {
		var val = $(this).val();
		var filename = val.split("\\").pop();
		$(this).siblings("span").text(filename);
	});
	$(".menu").click(function () {
		$("#container").addClass('resize');
		$(".f_image,.f_video").css({
			'position' : 'relative',
			'width' : '100%',
			'height' : '100%'
		});
		$("#container .con_wrap").velocity({
			height : '85%',
			width : '90%',
		}, {
			duration : 350,
			easing : "easeOutQuad"
		})
		$("#main-menu").velocity("fadeIn", {
			duration : 350
		});
		$("#main-menu section .wrap-sec").velocity("fadeIn", {
			duration : 350,
			delay : 100
		});
		$("#main-menu aside").delay(100).velocity({
			'marginLeft' : '0%'
		}, 350, 'easeOutQuad');
		return false;
	});
	$("#main-menu aside .close").click(function () {
		$("#main-menu aside").delay(100).velocity({
			'marginLeft' : '-33%'
		}, 350, 'easeOutQuad');
		$("#main-menu section .wrap-sec").velocity("fadeOut", {
			duration : 350
		});
		$("#main-menu").velocity("fadeOut", {
			duration : 350,
			delay : 500
		});
		$("#container").animate({
			'backgroundColor' : '#fff'
		}, 350);
		$("#container .con_wrap").velocity("reverse", {
			easing : "easeOutQuad",
			complete : function () {
				$("#container .con_wrap,#container").removeAttr("style");
				$("#container").removeClass('resize').show();
				$(".f_image,.f_video").removeAttr("style");
			}
		})
	});
	$.fn.animate_border = function () {
		var speed = 350;
		var time_delay = 0;
		var type_easing = "easeInOutQuad";
		$(this).wrapInner("<div class='w-border'></div>");
		$(this).children(".w-border").append("<div class='line top'></div><div class='line left'></div><div class='line bottom'></div><div class='line right'></div>");
		$("#home .items").hover(function () {
			$(this).find(".line.bottom").velocity({
				right : '-13px'
			}, speed, type_easing);
			$(this).find(".line.right").velocity({
				top : '-13px'
			}, {
				duration : speed,
				easing : type_easing,
				delay : time_delay
			});
			$(this).find(".line.top").velocity({
				left : '-13px'
			}, {
				duration : speed,
				easing : type_easing
			});
			$(this).find(".line.left").velocity({
				bottom : '-13px'
			}, {
				duration : speed,
				easing : type_easing,
				delay : time_delay
			});
		}, function () {
			$(this).find(".line.left").velocity("reverse", {
				easing : type_easing
			});
			$(this).find(".line.top").velocity("reverse", {
				delay : time_delay,
				easing : type_easing
			});
			$(this).find(".line.right").velocity("reverse", {
				easing : type_easing
			});
			$(this).find(".line.bottom").velocity("reverse", {
				delay : time_delay,
				easing : type_easing
			});
		})
	}
	$("#home .items .captions").animate_border();

	function inAnimateForm() {
		var id = $(this).attr('href');
		$(".popup").velocity({
			width : "100%",
			height : "2px"
		}, {
			duration : 600,
			easing : "easeOutQuad"
		});
		$(".popup").velocity({
			height : "100%"
		}, {
			duration : 400,			
		});
		$(".popup .close").velocity({
			right : 0
		}, {
			duration : 500,
			delay : 1000,
			easing : "easeInOutSine",
			complete : function () {
				$(".popup").css('overflow', 'auto');
			}
		})
	}
	$(".popup .close").click(function (e) {
		$(".popup .close").velocity("reverse", {
			complete : function () {
				$(".popup").css('overflow', 'hidden');
			}
		});
		$(".popup").velocity("reverse", {
			delay : 500
		});
		$(".popup").velocity({
			width : "0%",
			height : "1px"
		}, {
			duration : 500,			
			easing : "easeInQuart",
			complete : function () {
				$.fancybox.close();
				$("#step1,#step2").removeAttr("style");
				$("#step2").hide();
				$("#step-min").text(1)
			}
		});
	});
	$("#continueForm").click(function (e) {
		$("#step1").velocity({
			marginTop : "-100px",
			opacity : "0",
			display : "none"
		}, {
			duration : 500,
			easing : "easeOutQuart",
			complete : function () {
				$(this).hide();
				$("#step2").css({
					'opacity' : 0,
					'margin-top' : '100px',
					'display' : 'block'
				});
				$("#step-min").text(2)
			}
		});
		$("#step2").velocity({
			marginTop : "0px",
			opacity : "1",
			display : "block"
		}, {
			duration : 500,
			easing : "easeOutQuart",
			delay : 700,
		});
		return false;
	});
	function banner_content() {
		if ($("#banner-content").children("img").length == 1) {
			$("#banner-content").find("img").css("opacity", "0");
			var img_src = $("#banner-content").find("img").attr("src");
			$("#banner-content").css("background-image", 'url(' + img_src + ')');
		} else {
			$("#banner-content").height(910).css('overflow', 'hidden');
		}
	}
	$.fn.mouse_slide = function (e) {
		var time = 500;
		var easings = "easeOutSine";
		$("body").append("<audio src='images/ShutterSounds.mp3' id='soundEffect'></audio>");
		$(this).hover(function (e) {
			var el_pos = $(this).offset();
			var edge = closestEdge(e.pageX - el_pos.left, e.pageY - el_pos.top, $(this).width(), $(this).height());
			document.getElementById("soundEffect").play();
			if ($(this).children(".company-logo").length == 1) {
				$(this).children(".company-logo").velocity({
					top : "-50px",
					opacity : 0,
				}, {
					duration : 200,
				});
			}
			switch (log(edge)) {
			case "top":
				$(this).children(".hover").addClass("top");
				$(this).children(".hover").velocity({
					top : '0px',
				}, {
					duration : time,
					easing : easings
				});
				break;
			case "bottom":
				$(this).children(".hover").addClass("bottom");
				$(this).children(".hover").velocity({
					bottom : '0px',
				}, {
					duration : time,
					easing : easings
				});
				break;
			case "left":
				$(this).children(".hover").addClass("left");
				$(this).children(".hover").velocity({
					left : '0px',
				}, {
					duration : time,
					easing : easings
				});
				break;
			case "right":
				$(this).children(".hover").addClass("right");
				$(this).children(".hover").velocity({
					right : '0px',
				}, {
					duration : time,
					easing : easings
				});
				break;
				break;
			}
			$(this).children("img").velocity({
				scale : '1.1,1.1',
			}, {
				duration : 1000,
			});
		}, function (e) {
			var el_pos = $(this).offset();
			var edge = closestEdge(e.pageX - el_pos.left, e.pageY - el_pos.top, $(this).width(), $(this).height());
			switch (log(edge)) {
			case "top":
				if (log(edge) == "left" || log(edge) == "right"); {
					$(this).children(".hover").css('top', 0)
				}
				$(this).children(".hover").removeClass("top bottom left right");
				$(this).children(".hover").velocity({
					top : '-100%',
				}, {
					duration : time,
					easing : easings,
					complete : function () {
						$(this).removeAttr("style");
					}
				});
				break;
			case "bottom":
				if (log(edge) == "left" || log(edge) == "right"); {
					$(this).children(".hover").css('top', 0)
				}
				$(this).children(".hover").removeClass("top bottom left right");
				$(this).children(".hover").velocity({
					top : '100%',
				}, {
					duration : time,
					easing : easings,
					complete : function () {
						$(this).removeAttr("style");
					}
				});
				break;
			case "left":
				$(this).children(".hover").css('top', 0)
				$(this).children(".hover").removeClass("top bottom left right");
				$(this).children(".hover").velocity({
					left : '-100%',
				}, {
					duration : time,
					easing : easings,
					complete : function () {
						$(this).removeAttr("style");
					}
				});
				break;
			case "right":
				$(this).children(".hover").css('top', 0)
				$(this).children(".hover").removeClass("top bottom left right");
				$(this).children(".hover").velocity({
					left : '100%',
				}, {
					duration : time,
					easing : easings,
					complete : function () {
						$(this).removeAttr("style");
					}
				});
				break;
			}
			if ($(this).children(".company-logo").length == 1) {
				$(this).children(".company-logo").velocity({
					top : "0px",
					opacity : 1,
				}, {
					delay : 300,
					duration : 200,
				});
			}
			$(this).children("img").velocity("reverse", {
				duration : 500
			});
		});
	}
	//$(".img-thumb li").mouse_slide();
	$.fn.hoverGallery = function (config) {
		var setting = {
			'icon' : "images/material/icon-picture.png",
		};
		if (config) {
			$.extend(setting, config);
		}
		$(this).each(function (index, element) {
			var target = $(this).children("div").children("div");
			target.append("<div class=img ><img src='" + setting.icon + "'/></div>")
		});
		$("body").append("<audio src='images/ShutterSounds.mp3'></audio>");
		$(this).hover(function () {
			var parent = $(this);
			$("audio")[0].play();
			$(this).find(".company-logo").velocity({
				opacity : 0,
				marginTop : "-20px"
			}, {
				duration : 300,
			});
			$(this).children("img").velocity({
				scale : "1.1,1,1"
			}, {
				duration : 500
			});
			$(this).find(".hover").fadeIn(400);
			$(this).find(".wrap-text").velocity({
				top : "0px",
				opacity : 1
			}, {
				duration : 500,
				delay : 200,
				easing : "easeOutSine"
			});
			$(this).find(".img").velocity({
				marginTop : "20px",
				opacity : 1
			}, {
				duration : 500,
				delay : 200,
				easing : "easeOutSine"
			});
		}, function () {
			$(this).children("img").velocity("reverse", {
				duration : 500
			});
			$(this).find(".img").velocity("reverse");
			$(this).find(".company-logo").velocity("reverse");
			$(this).find(".wrap-text").velocity("reverse");
			$(this).find(".hover").delay(100).fadeOut();
		})
	}
	$(".main-gallery li").hoverGallery();
	$(".img-thumb li").hoverGallery({
		'icon' : 'images/material/icon-camera.png'
	});
	$(".box-getstart").hover(function () {
		$(this).velocity({
			backgroundColor : "#004b6f",
		}, {
			duration : 600,
			easing : "easeInOutSine"
		});
	}, function () {
		$(this).velocity("reverse");
	})
	$(".list-gallery.main_menu li").hover(function () {
		var parent = $(this);
		$(this).children(".company-logo").velocity({
			opacity : 0,
			top : '-70'
		}, {
			duration : 300,
		});
		parent.children(".hover").delay(200).fadeIn(300)
		parent.find(".img").velocity({
			opacity : 1,
			bottom : '0px'
		}, {
			duration : 500
		});

	}, function () {
		$(this).find(".img").velocity("reverse");
		$(this).children(".company-logo").velocity("reverse", {
			delay : 300
		});
		$(this).children(".hover").delay(300).fadeOut(500);
	});

	//icon menu hover
	$("header .menu").hover(function () {
		$(this).find("span").velocity({
			width : '47px'
		}, {
			duration : 500
		});
		$(this).siblings("span.left").hide();
	}, function () {
		$(this).find("span").velocity("reverse");
		$(this).siblings("span.left").show();
	});

	//icon contact hover
	$("#contact .list-contact li").hover(function () {
		$(this).find("img").velocity({
			bottom : '90px'
		}, {
			duration : 500,
			easing : "easeOutQuad"
		})
	}, function () {
		$(this).find("img").velocity("reverse");
	});
	$(".reset_location").click(function () {
		initialize(-6.192542, 106.797777);
		return false;
	});
	//scrool inline page
	$(".scrollPage li").click(function () {
		var target = $(this).find("a").attr('href');
		$("html,body").animate({
			scrollTop : $(target).offset().top - 70
		}, 500);
		return false;
	});
	//hover inquiry
	$("header .inquiry").hover(function(){
		$(this).find(".hover").velocity({
			width : '100%'
			},{
			duration : 200		
		})
		},function(){
		$(this).find(".hover").velocity("reverse");
	});

});