<?php $page = "about"; ?>
<?php include('inc_header.php');?>
    <!-- middle -->
    <section id="banner-content">
    <!--<img src="images/slider/banner-about.jpg" alt="">-->
    <div class="f_video">
    <video src="images/video/loop.mp4" loop preload="auto" autoplay></video></div>
      <div class="captions">
        <h2>YOUR BRAND VISUALIZED</h2>
      </div>
    </section>
    <div class="wrap-wide">
      <section class="view-column two content-wording">
        <div class=""><span class="tagline">FLIQ aims to empower your marketing campaign by delivering successful visual messages through photo and video production</span>
        </div>
        <div>
          <p>In this constantly evolving digital era that yields infinite  supply of information and entertainment, thepublic attention is evidently obscured.  Your message needs to be packaged carefully and effectively in order to grab  the attention of modern consumers. What better way to achieve that than through  visual imaging? A picture is indeed worth a thousand words. <br />
  FLIQ is a full-service production companystreamlining your  marketing strategy with visual excellence targeted to impactfully reach your  desired audience andconsumer base. We work from strategy building, design and  art direction, to delivering polished visuals that help your brand or business  speak louder and better across all media platforms.<br />
  <br />
  Our team consists of experienced creative minds, designers, developers,  strategists, marketers and passionate field executors and post-production team  put together with a mission not only to create stunning visual, but to optimize  your campaign and fulfill your business objectives with it.</p>
          </p>
        </div>
      </section>
    </div>
    <ul class="list-gallery afterclear img-thumb">
      <li><img src="images/content/img-thumb.jpg" alt="">
        <div class="hover"><div class="icon"></div></div>
      </li>
      <li><img src="images/content/img-thumb-06.jpg" alt="">
        <div class="hover"><div class="icon"></div></div>
      </li>
      <li><img src="images/content/img-thumb-07.jpg" alt="">
        <div class="hover"><div class="icon"></div></div>
      </li>
      <li><img src="images/content/img-thumb-08.jpg" alt="">
        <div class="hover"><div class="icon"></div></div>
      </li>
      <li><img src="images/content/img-thumb-10.jpg" alt="">
        <div class="hover"><div class="icon"></div></div>
      </li>
    </ul>
    <section id="client">
      <div class="wrap-wide">
        <div class="title">OUR CLIENTS</div>
        <p>Our one-stop visual shop caters to a clientele that spans over a wide range of industries. We work closely with our clients to design services that meet their image requirements across all media. </p>
        <ul class="afterclear">
          <li><img src="images/content/img-client.png" alt=""></li>
          <li><img src="images/content/img-client-02.png" alt=""></li>
          <li><img src="images/content/img-client-03.png" alt=""></li>
          <li><img src="images/content/img-client-04.png" alt=""></li>
          <li><img src="images/content/img-client-05.png" alt=""></li>
          <li><img src="images/content/img-client-06.png" alt=""></li>
          <li><img src="images/content/img-client-07.png" alt=""></li>
          <li><img src="images/content/img-client-08.png" alt=""></li>
          <li><img src="images/content/img-client-09.png" alt=""></li>
          <li><img src="images/content/img-client-10.png" alt=""></li>
          <li><img src="images/content/img-client.png" alt=""></li>
          <li><img src="images/content/img-client-02.png" alt=""></li>
          <li><img src="images/content/img-client-03.png" alt=""></li>
          <li><img src="images/content/img-client-04.png" alt=""></li>
          <li><img src="images/content/img-client-05.png" alt=""></li>
        </ul>
      </div>
    </section>
    <div class="box-getstart">
      <div class="wrap-wide">
        <p>Drive awareness to your brand withextraordinary visual content</p>
        <a href="#workwithus" class="btn white right fancybox">get started</a>
      </div>
    </div>
    <div class="view-column two">
      <div class="service">
        <div class="wrap"><span>Explore our</span>
          <div class="title">services</div>
          <p>Learn how you can boost your brand’s presence and engage with your audience through powerful visual content</p>
          <a href="service.php">explore now</a>
        </div>
      </div>
      <div class="service">
        <div class="wrap"><span>VISIT OUR</span>
          <div class="title">gallery</div>
          <p>We create outstanding still and moving images that meets the modern marketer’s needto stand out in this era of infinite choice</p>
          <a href="gallery-photo.php">photo</a>
          <a href="gallery-video.php">video</a>
        </div>
      </div>
    </div>
    
    <script>
    $(function(){		
		$("#home .items").hoverVideo();	
	})
    </script>
    <!-- end of middle -->
    <?php include('inc_footer.php');?>