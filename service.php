<?php $page = "services"; ?>
<?php include('inc_header.php');?>
    <!-- middle -->
    <section id="banner-content"><img src="images/slider/banner-service.jpg" alt="">
      <div class="captions">
        <h2>FLIQ OFFERS A ONE-STOP VISUAL PRODUCTION SOLUTION</h2>
      </div>
    </section>
    <section class="home-info">
      <div class="wrap-wide">
        <div class="main-title">services</div>
        <p>At FLIQ, we create genuine visual content that powerfully resonates with your consumer base. We realize that every project requires a different approach and we work closely with our clients to design services that meet their image requirements across all media. With our full in-house solutions, we are compelled to optimize our projects with the latest design trends and cutting-edge technology.</p>
        <ul class="icon-list afterclear scrollPage">
          <li>
            <div class="strategy"><a href="#strategy">link</a></div>
            <span>Strategy</span></li>
          <li>
            <div class="prepro"><a href="#pre-productions">link</a></div>
            <span>pre-production</span></li>
          <li>
            <div class="pro"><a href="#productions">link</a></div>
            <span>production</span></li>
          <li>
            <div class="digital"><a href="#digital">link</a></div>
            <span>digital marketing</span></li>
          <li>
            <div class="reporting"><a href="#reporting">link</a></div>
            <span>reporting</span></li>
        </ul>
      </div>
    </section>
    <section id="strategy" class="topline">
      <div class="wrap-wide">
        <section class="view-column two services">
          <div>
            <h3>Strategy Planning</h3>
            <p class="head">By evaluating all possible channels and opportunities, we determine the most effective strategy to achieve yourgoals and long-term objectives</p>
            <ul>
              <li>Research</li>
              <li>Competitor & Consumer Analysis</li>
              <li>Audience-targeted Marketing Plan</li>
            </ul>
          </div>
        </section>
      </div>
    </section>
    <section id="pre-productions" class="topline">
      <div class="wrap-wide">
        <section class="view-column two services">
        <div><img src="images/content/img-brain.png" alt="" style="width:560px"></div>
          <div>
            <h3>PRE-PRODUCTION</h3>
            <p class="head">We develop big ideas to perfectly convey your brand story and drive a deeper engagement through quality photography or video

</p>
            <ul>
              <li>Creative Concept & Design</li>
              <li>Storyboard</li>
              <li>Client Engagement</li>
            </ul>
          </div>
        </section>
      </div>
    </section>
    <section id="productions" class="topline">
      <div class="wrap-wide">
        <section class="view-column two services">        
          <div>
            <h3>PRODUCTION</h3>
            <p class="head">Our combined skill set enables us to applyboundless creativity and optimized marketing strategy to produce succesful photography& videosolution</p>            
            <ul>
              <li>Photo Shoot</li>
              <li>Video Shoot</li>
              <li>Complete Post Production</li>
              <li>Delivery Finished Product </li>
            </ul>
          </div>
          <div><p align="right"><img src="images/content/img-camera.png" alt="" style="width:560px"></p></div>
        </section>
      </div>
    </section>
    <section id="digital" class="topline">
      <div class="wrap-wide">
        <section class="view-column two services">
        <div></div>
          <div>
            <h3>DIGITAL MARKETING</h3>
            <p class="head">We implement a holistic digital marketing solution tailored to deliver your business objectives.We base our strategy and execution on </p>            
            <ul>
              <li>Brand Activation</li>
              <li>Website & Mobile Development</li>
              <li>Social Media Optimization</li>
              <li>Social Media Campaign</li>
              <li>Media Planning & Buying</li>
            </ul>
            <p>
              <a href="#"><img src="images/content/icon-webarq.png" alt=""></a>
            </p>
          </div>
        </section>
      </div>
    </section>
    <section id="reporting" class="topline">
      <div class="wrap-wide">
        <section class="view-column two services">        
          <div>
            <h3>REPORTING</h3>
            <p class="head">We provide and analyze data that enable us to craft increasingly personalized visual content to maximize engagement and interaction with your audience</p>            
            <ul>
              <li>Conversion Rate Optimization</li>
              <li>Measurement & Analytics</li>              
            </ul>
          </div>
          <div><p align="right"><img src="images/content/img-report.png" alt="" style="width:560px"></p></div>
        </section>
      </div>
    </section>
    <div class="box-getstart">
      <div class="wrap-wide">
        <p>Craft powerful storytelling through beautiful still or moving images for your brand</p>
        <a href="#letstalk" class="btn white right fancybox">get started</a>
      </div>
    </div>
    <div class="view-column two">
      <div class="service">
        <div class="wrap"><span>Explore our</span>
          <div class="title">services</div>
          <p>Learn how you can boost your brand’s presence and engage with your audience through powerful visual content</p>
          <a href="service.php">explore now</a>
        </div>
      </div>
      <div class="service">
        <div class="wrap"><span>VISIT OUR</span>
          <div class="title">gallery</div>
          <p>We create outstanding still and moving images that meets the modern marketer’s needto stand out in this era of infinite choice</p>
          <a href="gallery-photo.php">photo</a>
          <a href="gallery-video.php">video</a>
        </div>
      </div>
    </div>
    <!-- end of middle -->
    <?php include('inc_footer.php');?>