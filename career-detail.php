<?php $page = "careers"?>
<?php include('inc_header.php');?>
    <!-- middle -->
    <!--<div class="slider-wrapper photo-detail cap-only">
      <div id="slider" class="nivoSlider">
        <img src="images/slider/banner-career-detail.jpg" alt="" title="#htmlcaption" />
        <img src="images/slider/banner-photography2.jpg" alt="" title="#htmlcaption2" />
        <img src="images/slider/banner-career-detail.jpg" alt="" title="#htmlcaption" />
        <img src="images/slider/banner-photography2.jpg" alt="" title="#htmlcaption2" />
      </div>
      <div id="htmlcaption" class="nivo-html-caption">
        <div class="text">We always keep an eye out for talented people to join us. </div>
      </div>
      <div id="htmlcaption2" class="nivo-html-caption">
        <div class="text">We always keep an eye out for talented people to join us. </div>
      </div>
      <nav>
        <a href="#" class="more">more</a>
        <a href="#" class="share">share</a>
      </nav>
    </div>-->
    <section id="banner-content"><img src="images/slider/banner-career-detail.jpg" alt="">
      <div class="captions">        
        <h2>We always keep an eye out for talented people to join us.</h2>        
      </div>
    </section>
    <nav id="banner">
      <div class="left">
        <a href="#" class="more">more</a>
        <a href="#" class="share">share</a>
      </div>      
      <div class="right">
        <a href="#" class="prev">prev</a>
        <a href="#" class="next">next</a>
      </div>
    </nav>
    <div class="wrap-wide">
      <section class="view-column two content-wording">
        <div class="">
          <h5>JOB POSITION</h5>
          <span class="tagline">editorial<br>
          photographer</span>
          <p style="margin-top:15px;">Send us your PDF resume, a link to your online<br />
            portfolio and salary requirements with Editorial<br />
            Photogapher in the subject line.</p>
          <a href="#applynow" class="btn blue fancybox anim" style="margin-top:20px;"><span>Apply NOW</span></a>
        </div>
        <div>
          <div class="qualification">
            <h3>Responsibilities</h3>
            <ul>
              <li>Meeting and liaising with clients to discuss and identify their needs</li>
              <li> Working with internal team to devise a digital marketing campaign that meets the client’s brief and budget</li>
              <li> Working with Project Manager to brief the internal team on ongoing projects</li>
              <li> Working on pitches, alongside the team, to present creative concept or campaign strategies to the client</li>
              <li> Presenting campaign pitches and costs to clients</li>
              <li> Reviewing and presenting works to client for approval or modification</li>
              <li> Monitoring work progress and keeping in contact with clients at all stages</li>
              <li> Check the status of each project and ensure timely project completion</li>
              <li> Arranging and attending meetings </li>
            </ul>
            <h3>Qualification</h3>
            <ul>
              <li>Clear knowledge in digital marketing and web technologies</li>
              <li> Must possess at least a Bachelor’s Degree, Master’s Degree/ </li>
              <li>Post Graduate Degree, Art/ Design/ Creative Multimedia, Advertising/ Media, Computer Sciene/ </li>
              <li>Information Technology, Business Studies/ Administration/ Management or equivalent.
                Required languages: Bahasa Indonesia &amp; English</li>
              <li> Ability to communicate clearly both verbally and in writing</li>
              <li> Ability to manage multiple projects in an organized manner</li>
              <li> Strong interpersonal skills and the ability to work with a wide range of people</li>
              <li> Professional appearance and persona</li>
              <li> Willing to travel regularly to meet clients </li>
            </ul>
          </div>
        </div>
      </section>
    </div>    
    <div class="box-getstart">
      <div class="wrap-wide">
        <p>Deliver your message and empower your brand through photo & video production</p>
        <a href="#workwithus" class="btn white right fancybox">get started</a>
      </div>
    </div>
    <!--inline html-->
    <div style="display:none">
      <div id="applynow" class="popup">
      <div class="close"><img src="images/material/btn-close.png" alt=""></div>
        <div class="wrap_popup">
          <div class="title">apply resume</div>
          <p>EDITORIAL PHOTOGRAPHER</p>
          <form action="" method="post">
            <div class="row">
              <label>What ‘s your name?</label>
              <input name="" type="text" value="">
              <span>*</span>
            </div>
            <div class="row">
              <label>Your business name?</label>
              <input name="" type="text" value="">
              <span>*</span>
            </div>
            <div class="row">
              <label>What's your email address?</label>
              <input name="" type="text" value="">
              <span>*</span>
            </div>
            <div class="row">
              <label>Best contact phone number?</label>
              <input name="" type="text" value="">
              <span>*</span>
            </div>
            <div class="row">
              <label>What last your position?</label>
              <input name="" type="text" value="">
              <span>*</span>
            </div>
            <div class="row">
              <label>How much do you want to expected salary?</label>
              <input name="" type="text" value="">
              <span>*</span>
            </div>
            <div class="row">
              <label>Upload your CV</label>
              <div class="wrap-file"><input name="" type="file"><span></span></div>
              <span><img src="images/material/icon-file.png" alt=""></span>
            </div>
            <div class="row">
              <label>Upload your portfolio</label>
              <div class="wrap-file"><input name="" type="file"><span></span></div>
              <span><img src="images/material/icon-file.png" alt=""></span>
            </div>
            <input name="" type="submit" value="apply now" class="btn blue">
          </form>
        </div>
      </div>      
    </div>
    <!--end inline html-->
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
			directionNav: true,
			controlNav: false,			
		});		
    });
    </script>
    <!-- end of middle -->
    <?php include('inc_footer.php');?>