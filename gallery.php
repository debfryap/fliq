<?php $page = "Gallery"; ?>
<?php include('inc_header.php');?>
    <!-- middle -->
    <section id="home" class="afterclear">
      <div class="items">
        <div class="f_image"><img src="images/slider/banner-gallery.jpg" alt=""/></div>
        <div class="captions">
          <h2>
            <a href="gallery-photo.php">PHOTO</a>
          </h2>
        </div>
      </div>
      <div class="items">
        <div class="f_video"><!--
        <video loop preload="auto" style="width: 100%; height: auto;" id="videoLoop">
          <source src="images/video/films.webm" type="video/webm">
          <source src="images/video/films.ogv" type="video/ogg">
          <source src="images/video/films.mp4" type="video/mp4">
        </video>-->
          <video src="images/video/films.mp4" loop preload="auto" id="videoLoop"></video>
        </div>
        <div class="captions">
          <h2>
            <a href="gallery-video.php">Video</a>
          </h2>
        </div>
      </div>
    </section>
    <section id="main-gallery">
      <div class="title">gallery</div>
      <div>
        <a href="gallery.php" class="btn sort active"><span>ALL</span></a>
        <a href="gallery-photo.php" class="btn sort"><span>PHOTO</span></a>
        <a href="gallery-video.php" class="btn sort"><span>VIDEO</span></a>
      </div>
      <ul class="list-gallery afterclear main-gallery">
        <li><img src="images/content/img-gallery.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company.png" alt=""></div>
          <div class="hover with-text">
            <a href="photo-detail.php">link</a>
            <div class="wrap-text">
              <div class="name">Sensl</div>
              <div class="main-cap">This is Photoshop's version  of Lorem Ipsum.</div>
              <div class="time">January 2014</div>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-gallery-02.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-02.png" alt=""></div>
          <div class="hover with-text">
            <a href="photo-detail.php">link</a>
            <div class="wrap-text">
              <div class="name">Sensl</div>
              <div class="main-cap">This is Photoshop's version  of Lorem Ipsum.</div>
              <div class="time">January 2014</div>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-gallery-03.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-03.png" alt=""></div>
          <div class="hover with-text">
            <a href="photo-detail.php">link</a>
            <div class="wrap-text">
              <div class="name">Sensl</div>
              <div class="main-cap">This is Photoshop's version  of Lorem Ipsum.</div>
              <div class="time">January 2014</div>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-gallery.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company.png" alt=""></div>
          <div class="hover with-text">
            <a href="photo-detail.php">link</a>
            <div class="wrap-text">
              <div class="name">Sensl</div>
              <div class="main-cap">This is Photoshop's version  of Lorem Ipsum.</div>
              <div class="time">January 2014</div>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-gallery-02.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-02.png" alt=""></div>
          <div class="hover with-text">
            <a href="photo-detail.php">link</a>
            <div class="wrap-text">
              <div class="name">Sensl</div>
              <div class="main-cap">This is Photoshop's version  of Lorem Ipsum.</div>
              <div class="time">January 2014</div>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-gallery-03.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-03.png" alt=""></div>
          <div class="hover with-text">
            <a href="photo-detail.php">link</a>
            <div class="wrap-text">
              <div class="name">Sensl</div>
              <div class="main-cap">This is Photoshop's version  of Lorem Ipsum.</div>
              <div class="time">January 2014</div>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-gallery.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company.png" alt=""></div>
          <div class="hover with-text">
            <a href="photo-detail.php">link</a>
            <div class="wrap-text">
              <div class="name">Sensl</div>
              <div class="main-cap">This is Photoshop's version  of Lorem Ipsum.</div>
              <div class="time">January 2014</div>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-gallery-02.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-02.png" alt=""></div>
          <div class="hover with-text">
            <a href="photo-detail.php">link</a>
            <div class="wrap-text">
              <div class="name">Sensl</div>
              <div class="main-cap">This is Photoshop's version  of Lorem Ipsum.</div>
              <div class="time">January 2014</div>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-gallery-03.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-03.png" alt=""></div>
          <div class="hover with-text">
            <a href="photo-detail.php">link</a>
            <div class="wrap-text">
              <div class="name">Sensl</div>
              <div class="main-cap">This is Photoshop's version  of Lorem Ipsum.</div>
              <div class="time">January 2014</div>
            </div>
          </div>
        </li>
      </ul>
    </section>
    <div id="loadmore"><span>load more</span></div>
    <div class="box-getstart">
      <div class="wrap-wide">
        <p>BRING YOUR STORIES TO LIFE WITH OUR PHOTO & VIDEO PRODUCTION</p>
        <a href="#workwithus" class="btn white right fancybox">LET’S WORK TOGETHER</a>
      </div>
    </div>
    <script>
    $(function(){		
		$("#home .items").hoverVideo();	
	})
    </script>
    <!-- end of middle -->
    <?php include('inc_footer.php');?>