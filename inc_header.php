<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>WEBARQ - Static Website</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="icon" href="favicon.ico">
<!--Style-->
<link rel="stylesheet" href="css/reset.css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/nivo-slider.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" href="css/style.css">

<!--js-->
<script src="js/vendor/jquery-1.9.1.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/vendor/modernizr-2.6.2.min.js"></script>
<script src="js/velocity.min.js"></script>
<script src="js/velocity.ui.js"></script>
<script src="js/jquery.nivo.slider.pack.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<script src="js/jquery_function.js"></script>
<script src="js/mouseover.js"></script>
</head>
<body>
<!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser.<br>Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="https://developers.google.com/chrome/chrome-frame/">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<!-- Add your site or application content here -->

<!-- header -->
<div id="container">
<div id="main-menu">
  <aside>
    <div class="close"><img src="images/material/btn-close.png" alt=""/></div>
    <nav>
      <ul>
      <li>
          <a href="index.php">home</a>
        </li>
        <li>
          <a href="about.php">about</a>          
        </li>
        <li>
          <a href="service.php">services</a>
        </li>
        <li>
          <a href="gallery.php">gallery</a>
          <ul>
            <li>
              <a href="gallery-photo.php">Photo</a>
            </li>
            <li>
              <a href="gallery-video.php">Video</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="blog.php">blog</a>
        </li>
        <li>
          <a href="contact.php">contact</a>
          <ul>
            <li>
              <a href="#workwithus" class="fancybox">Start Your Project</a>
            </li>
            <li>
              <a href="#letstalk" class="fancybox">Say Hello</a>
            </li>
            <li>
              <a href="career.php">Career</a>
            </li>
          </ul>
        </li>
      </ul>      
    </nav>
    <div class="socmed">
        <a href="#"><img src="images/material/icon-socmed-menu.png"  alt=""/></a>
        <a href="#"><img src="images/material/icon-socmed-menu-02.png"  alt=""/></a>
        <a href="#"><img src="images/material/icon-socmed-menu-03.png"  alt=""/></a>
        <a href="#"><img src="images/material/icon-socmed-menu-04.png"  alt=""/></a>
        <a href="#"><img src="images/material/icon-socmed-menu-05.png"  alt=""/></a>
      </div>
  </aside>
  <section>
    <div class="wrap-sec">
      <h4>Latest gallery</h4>
      <ul class="list-gallery afterclear main_menu">
        <li><img src="images/content/img-gallery.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company.png" alt=""></div>
          <div class="hover">
            <div>
              <a href="gallery.php">link</a><div class="img"><img src="images/material/icon-picture.png" alt=""></div>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-gallery-02.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-02.png" alt=""></div>
          <div class="hover">
            <div><a href="gallery.php">link</a><div class="img"><img src="images/material/icon-picture.png" alt=""></div></div>
          </div>
        </li>
        <li><img src="images/content/img-gallery-03.jpg" alt="">
          <div class="company-logo"><img src="images/content/img-logo-company-03.png" alt=""></div>
          <div class="hover">
            <div><a href="gallery.php">link</a><div class="img"><img src="images/material/icon-picture.png" alt=""></div></div>
          </div>
        </li>
      </ul>
      <h4>from our blog</h4>
      <ul class="list-blog">
        <li>
          <h3>
            <a href="#">This is Photoshop's version  of Lorem IpsumProin gravida nibh vel velit<br>
              auctor aliquet.</a>
          </h3>
          <span>Jan 11, 2014</span></li>
        <li>
          <h3>
            <a href="#">This is Photoshop's version  of Lorem IpsumProin gravida nibh vel velit<br>
              auctor aliquet.</a>
          </h3>
          <span>Jan 11, 2014</span></li>
        <li>
          <h3>
            <a href="#">This is Photoshop's version  of Lorem IpsumProin gravida nibh vel velit<br>
              auctor aliquet.</a>
          </h3>
          <span>Jan 11, 2014</span></li>
      </ul>
    </div>
  </section>
</div>
<div class="con_wrap">
<header>
  <a href="#" class="menu"><img src="images/material/icon-menu.png" alt=""/> <span></span></a>
  <span class="left"><?php echo $page; ?></span>
  <a href="index.php" class="logo"><img src="images/material/logo.png" alt=""/></a>
  <a href="#letstalk" class="inquiry fancybox"><span>Inquiry</span><span class="hover"></span></a>
</header>
<!-- end of header -->