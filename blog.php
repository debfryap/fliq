<?php $page = "Gallery"; ?>
<?php include('inc_header.php');?>
    <!-- middle -->
    <div class="wrap-wide">
      
      <section class="view-column two content-wording">
      <div class="image"><img src="images/content/img-blog.jpg" alt=""></div>
        <div><span class="tagline">This is Photoshop's version  of Lorem<br>
          Ipsum. </span>
          <div class="info">
            <span class="date">Jan 24, 2014</span>
            <div class="category">In <span>Category </span> by <strong>Mr. Lorem Ipsum</strong></div>
          </div>
        </div>
        <div>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justoullam ac urna eu felis dapibus condimentum. </p>
          <a href="#" class="link-continue">continue reading</a>
        </div>
      </section>
    </div>
    <hr size="1px" color="#d8d8d8">
    <div class="wrap-wide">
      <section class="view-column two content-wording">
        <div><span class="tagline">This is Photoshop's version  of Lorem<br>
          Ipsum. </span>
          <div class="info">
            <span class="date">Jan 24, 2014</span>
            <div class="category">In <span>Category </span> by <strong>Mr. Lorem Ipsum</strong></div>
          </div>
        </div>
        <div>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justoullam ac urna eu felis dapibus condimentum. </p>
          <a href="#" class="link-continue">continue reading</a>
        </div>
      </section>
    </div>
    <hr size="1px" color="#d8d8d8">
    <div class="wrap-wide">
      
      <section class="view-column two content-wording">
      <div class="image"><img src="images/content/img-blog-06.jpg" alt=""></div>
        <div><span class="tagline">This is Photoshop's version  of Lorem<br>
          Ipsum. </span>
          <div class="info">
            <span class="date">Jan 24, 2014</span>
            <div class="category">In <span>Category </span> by <strong>Mr. Lorem Ipsum</strong></div>
          </div>
        </div>
        <div>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justoullam ac urna eu felis dapibus condimentum. </p>
          <a href="#" class="link-continue">continue reading</a>
        </div>
      </section>
    </div>
    <hr size="1px" color="#d8d8d8">
    <div class="wrap-wide">
      
      <section class="view-column two content-wording">
      <div class="image"><img src="images/content/img-blog-08.jpg" alt=""></div>
        <div><span class="tagline">This is Photoshop's version  of Lorem<br>
          Ipsum. </span>
          <div class="info">
            <span class="date">Jan 24, 2014</span>
            <div class="category">In <span>Category </span> by <strong>Mr. Lorem Ipsum</strong></div>
          </div>
        </div>
        <div>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justoullam ac urna eu felis dapibus condimentum. </p>
          <a href="#" class="link-continue">continue reading</a>
        </div>
      </section>
    </div>
    <hr size="1px" color="#d8d8d8">
    <div class="wrap-wide">
      <section class="view-column two content-wording">
        <div><span class="tagline">This is Photoshop's version  of Lorem<br>
          Ipsum. </span>
          <div class="info">
            <span class="date">Jan 24, 2014</span>
            <div class="category">In <span>Category </span> by <strong>Mr. Lorem Ipsum</strong></div>
          </div>
        </div>
        <div>
          <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justoullam ac urna eu felis dapibus condimentum. </p>
          <a href="#" class="link-continue">continue reading</a>
        </div>
      </section>
    </div>
    <div id="loadmore"><span>load more</span></div>
    
    <!-- end of middle -->
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
			directionNav: true,
			controlNav: false,			
		});
		$('#slider-image').nivoSlider({
			directionNav: true,
			controlNav: false,			
		});
    });
    </script>
    <?php include('inc_footer.php');?>